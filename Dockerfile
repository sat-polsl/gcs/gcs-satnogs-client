FROM debian:11.5-slim

RUN apt-get update && apt-get install -y \
    gnupg2 \
    wget

COPY etc /etc/

ENV APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=DontWarn

WORKDIR /var/satnogs/

RUN wget -qO - https://build.opensuse.org/projects/home:librespace:satnogs/public_key \ 
    | apt-key add - \
    && apt-get update && apt-get install -y \
#   install dependencies
    socat \
    python3-pip \ 
    python3-apt \ 
#   satnogs-flowgraph dependencies
    soapysdr-tools \
    cmake \
    build-essential \
    gnuradio \
    libgmp-dev \
    gnuradio-dev \
    gr-satnogs \
#   gr-soapy dependencies
    libboost-dev \
    libboost-date-time-dev \
    libboost-filesystem-dev \
    libboost-program-options-dev \
    libboost-system-dev \
    libboost-thread-dev \
    libboost-regex-dev \
    libboost-test-dev \
    python3-six \
    python3-mako \
    python3-dev \
    swig \
    gcc \
    gnuradio-dev \
    libsoapysdr-dev \
    libconfig++-dev \
    libgmp-dev \
    liborc-0.4-0 \
    liborc-0.4-dev \
    liborc-0.4-dev-bin \
    git \
#   satnogs-client dependencies
    python3-libhamlib2 \
    zlib1g-dev \
    libjpeg-dev \
    libhdf5-dev \
    libhdf5-103 \
    gpsd \
    gpsd-clients \ 
    gpsd-tools \
    && pip3 install cython gps \
#   install gr-soapy
    && git clone https://gitlab.com/librespacefoundation/gr-soapy \
    && cd gr-soapy \
    && mkdir build \
    && cd build \
    && cmake .. \
    && make -j $(nproc --all) \
    && make install \
    && ldconfig \
    && cd ../.. \
    && rm -rf gr-soapy \ 
#   install satnogs-flowgraph
    && git clone https://gitlab.com/librespacefoundation/satnogs/satnogs-flowgraphs.git \
    && cd satnogs-flowgraphs \
    && mkdir build \
    && cd build \
    && cmake .. \
    && make PYTHONPATH=/usr/lib/x86_64-linux-gnu/:/usr/lib/arm-linux-gnueabihf/:/usr/lib/aarch64-linux-gnu \
    && make install \
    && cd ../.. \
    && rm -rf satnogs-flowgraphs \
#   install satnogs-client
    && pip3 install satnogs-client \
#   install uhd images
    && uhd_images_downloader

COPY opt /opt/
COPY docker-entrypoint.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["satnogs-client"]

