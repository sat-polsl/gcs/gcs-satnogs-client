# Start
`docker run --privileged -v /var/run/dbus:/var/run/dbus -v /var/run/avahi-daemon/socket:/var/run/avahi-daemon/socket --name gcs-satnos-client -it gcs-satnogs-client-image`

or

`docker-compose up -t`

# Satnogs-client configuration
Satnogs-client can be configured in two ways:
1. Define configuration variables as KEY=VALUE pairs in `opt/satnogs-client/satnogs-client.conf` before building image.
or
2. Define envrionment variables in gcs-satnogs-client service in `docker-compose.yml`

All configuration variables are listed and described [here](https://wiki.satnogs.org/SatNOGS_Client_Setup)

Variables in `docker-compose.yml` overrides variables defined in `satnogs-client.conf`

