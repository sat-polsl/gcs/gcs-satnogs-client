#!/usr/bin/env python3
import argparse
import Hamlib
from os import environ

def tune_rotator():
    rot_model = environ.get('SATNOGS_ROT_MODEL', 'ROT_MODEL_DUMMY')
    rot_port = environ.get('SATNOGS_ROT_PORT', '/dev/ttyUSB0')
    rot_baud = int(environ.get('SATNOGS_ROT_BAUD', 19200))

    print('Rotator model: {}'.format(rot_model))
    print('Rotator port: {}'.format(rot_port))
    print('Rotator baud: {}'.format(rot_baud))

    rot = Hamlib.Rot(getattr(Hamlib, rot_model))
    rot.state.rotport.pathname = rot_port
    rot.state.rotport.parm.serial.rate = rot_baud

    rot.open()

    rot.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--freq', type=int)
    parser.add_argument('--tle', type=str)
    parser.add_argument('--timestamp', type=str)
    parser.add_argument('--id', type=int)
    parser.add_argument('--baud', type=float)
    parser.add_argument('--script_name', type=str)
    args = parser.parse_args()

    print('Preobservation script')
    print('Frequency: {}'.format(args.freq))
    print('TLE: {}'.format(args.tle))
    print('Timestamp: {}'.format(args.timestamp))
    print('ID: {}'.format(args.id))
    print('Baud: {}'.format(args.baud))
    print('Script name: {}'.format(args.script_name))

    tune_rotator()


if __name__ == '__main__':
    main()
