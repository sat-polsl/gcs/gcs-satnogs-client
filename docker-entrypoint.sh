#!/bin/bash

set -e

SATNOGS_CLIENT_CONFIG_FILE=/opt/satnogs-client/satnogs-client.conf
export PYTHONPATH=/usr/local/lib/python3/dist-packages
export PYTHONUNBUFFERED=TRUE
export SATNOGS_PRE_OBSERVATION_SCRIPT="/opt/satnogs-client/preobservation.py --freq {{FREQ}} --tle {{TLE}} --timestamp {{TIMESTAMP}} --id {{ID}} --script_name {{SCRIPT_NAME}}" 
export SATNOGS_POST_OBSERVATION_SCRIPT="/opt/satnogs-client/postobservation.py --freq {{FREQ}} --tle {{TLE}} --timestamp {{TIMESTAMP}} --id {{ID}} --script_name {{SCRIPT_NAME}}" 

if [[ -n ${RIGCTLD_HOST} ]]; then
  socat TCP-LISTEN:4532,fork TCP:${RIGCTLD_HOST}:${RIGCTLD_PORT} &
fi

awk_script='BEGIN{
  c=0;
  for(v in ENVIRON) c++;
  i=0;
  for(v in ENVIRON) {
    if (v!="_") {
      printf "%s",v; if (i<c-1) printf "%s","\|"
    };
    i++
  }
}'

export $(grep -v '^#' $SATNOGS_CLIENT_CONFIG_FILE | grep -v $(awk "$awk_script") | xargs)

if [[ ${SATNOGS_SOAPY_RX_DEVICE} == *"uhd"* ]]; then
  echo "UHD configuration"
  uhd_find_devices 
  status=$?
  if [ $status -ne 0 ]; then
    echo "UHD not configured yet"
    echo "Restarting container!"
    exit
  else 
    SoapySDRUtil --probe=driver=uhd
  fi
fi

echo "Starting satnogs-client"
exec "$@"
